package br.com.fdw.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.fdw.entities.Aluno;
import br.com.fdw.entities.Observer;

@Repository
public class AlunoDAO extends AbstractDAO<Aluno> {

	@Override
	public Class<Aluno> entityClass() {
		return Aluno.class;
	}

	public List<Observer> listarAlunosDaTurmaObserver(long turmaID) {
		return manager.createQuery("SELECT d.aluno FROM Desempenho d WHERE d.turma.id = " + Long.toString(turmaID))
				.getResultList();
	}

	public List<Aluno> buscaPorNome(String nome) {
		nome = "%" + nome + "%";
		String query = "SELECT a FROM Aluno a WHERE UPPER(a.nome) LIKE UPPER(?1)";
		return manager.createQuery(query).setParameter(1, nome).getResultList();
	}

}
