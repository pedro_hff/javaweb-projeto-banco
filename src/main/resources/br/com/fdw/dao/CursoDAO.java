package br.com.fdw.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.fdw.entities.Curso;

@Repository
public class CursoDAO extends AbstractDAO<Curso> {

	@Override
	public Class<Curso> entityClass() {
		return Curso.class;
	}

	public List<Curso> buscaPorNome(String nome) {
		nome = "%" + nome + "%";
		String query = "SELECT c FROM Curso c WHERE UPPER(c.nome) LIKE UPPER(?1)";
		return manager.createQuery(query).setParameter(1, nome).getResultList();
	}

}
