package br.com.fdw.dao;

import java.math.BigInteger;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.fdw.entities.Aluno;
import br.com.fdw.entities.Desempenho;
import br.com.fdw.entities.Turma;

@Repository
public class DesempenhoDAO extends AbstractDAO<Desempenho> {

	@Override
	public Class<Desempenho> entityClass() {
		return Desempenho.class;
	}

	public List<Desempenho> listarDesempenhoDaTurma(Long id) {
		return manager.createQuery("SELECT d FROM Desempenho d WHERE d.turma = " + id).getResultList();
	}

	public Desempenho buscarPorAlunoeTurma(Long alunoID, Long turmaID) {
		return (Desempenho) manager
				.createQuery("SELECT d FROM Desempenho d WHERE d.aluno = " + alunoID + " AND d.turma = " + turmaID)
				.getSingleResult();
	}

	public int quantidadeAlunosNaTurma(Long turmaID) {
		String query = "SELECT COUNT(*) FROM desempenho WHERE desempenho.turma_id = " + Long.toString(turmaID);
		BigInteger bi = (BigInteger) manager.createNativeQuery(query).getSingleResult();
		return bi.intValue();
	}

	public List<Turma> listaTurmasdoAluno(Aluno aluno) {
		String query = "SELECT d.turma FROM Desempenho d WHERE d.aluno.id = " + aluno.getId();
		return manager.createQuery(query).getResultList();
	}

}