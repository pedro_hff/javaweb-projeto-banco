package br.com.fdw.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.fdw.entities.Disciplina;

@Repository
public class DisciplinaDAO extends AbstractDAO<Disciplina> {

	@Override
	public Class<Disciplina> entityClass() {
		return Disciplina.class;
	}

	public List<Disciplina> buscarPorNome(String nome) {
		nome = "%" + nome + "%";
		String query = "SELECT d FROM Disciplina d WHERE UPPER(d.nome) LIKE UPPER(?1) ";
		return manager.createQuery(query).setParameter(1, nome).getResultList();
	}

}
