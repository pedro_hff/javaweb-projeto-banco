package br.com.fdw.dao;

import org.springframework.stereotype.Repository;

import br.com.fdw.entities.Endereco;

@Repository
public class EnderecoDAO extends AbstractDAO<Endereco> {

	@Override
	public Class<Endereco> entityClass() {
		return Endereco.class;
	}

}
