package br.com.fdw.dao;

import org.springframework.stereotype.Repository;

import br.com.fdw.entities.Nota;

@Repository
public class NotaDAO extends AbstractDAO<Nota> {

	@Override
	public Class<Nota> entityClass() {
		return Nota.class;
	}

}
