package br.com.fdw.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.fdw.entities.Professor;

@Repository
public class ProfessorDAO extends AbstractDAO<Professor> {

	@Override
	public Class<Professor> entityClass() {
		return Professor.class;
	}

	public List<Professor> buscaPorNome(String nome) {
		nome = "%" + nome + "%";
		String query = "SELECT p FROM Professor p WHERE UPPER(p.nome) LIKE UPPER(?1)";
		return manager.createQuery(query).setParameter(1, nome).getResultList();
	}
}
