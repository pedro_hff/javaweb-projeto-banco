package br.com.fdw.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.fdw.entities.Curso;
import br.com.fdw.entities.Turma;

@Repository
public class TurmaDAO extends AbstractDAO<Turma> {

	@Override
	public Class<Turma> entityClass() {
		return Turma.class;
	}

	public List<Turma> buscaPorCurso(Curso c) {
		String query = "SELECT t FROM Turma t WHERE t.disciplina.curso.id = " + c.getId();
		return manager.createQuery(query).getResultList();
	}

}
