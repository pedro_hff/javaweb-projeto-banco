package br.com.fdw.dao;

import org.springframework.stereotype.Repository;

import br.com.fdw.entities.Usuario;

@Repository
public class UsuarioDAO extends AbstractDAO<Usuario> {

	@Override
	public Class<Usuario> entityClass() {
		return Usuario.class;
	}

	public Usuario findByLogin(String login) {
		try {
			return (Usuario) manager.createQuery("SELECT U FROM Usuario U WHERE U.login = :log")
					.setParameter("log", login).getSingleResult();
		} catch (Exception e) {
			System.out.println("SELECT U FROM Usuario U WHERE U.login = :log");
			System.out.println("Usuario de login " + login + " não encontrado.");
		}
		return null;
	}

}
