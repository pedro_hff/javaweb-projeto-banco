package br.com.fdw.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Aluno extends Usuario {

	@Temporal(TemporalType.DATE)
	private Date datavinculo;

	@ManyToOne
	private Curso curso;

	@OneToMany(mappedBy = "aluno")
	private List<Desempenho> desempenhos;

	public Aluno() {
	}

	// Getters & Setters
	public Date getDatavinculo() {
		return datavinculo;
	}

	public void setDatavinculo(Date datavinculo) {
		this.datavinculo = datavinculo;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<Desempenho> getDesempenhos() {
		return desempenhos;
	}

	public void setDesempenhos(List<Desempenho> desempenhos) {
		this.desempenhos = desempenhos;
	}

}
