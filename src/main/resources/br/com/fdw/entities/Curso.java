package br.com.fdw.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "curso_id", sequenceName = "curso_seq")
public class Curso extends AbstractEntity implements Observer {
 
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	private String nome;
	private String tipo;

	// Observer
	private String notificacao;
	private boolean notificacaoLida;

	@OneToMany(mappedBy = "curso")
	private List<Disciplina> disciplinas;

	@ManyToMany
	private List<Professor> professores;

	@OneToMany(mappedBy = "curso")
	private List<Aluno> alunos;

	public Curso() {
	}

	// Getters & Setters

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}

	public List<Professor> getProfessores() {
		return professores;
	}

	public void setProfessores(List<Professor> professores) {
		this.professores = professores;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getNotificacao() {
		return notificacao;
	}

	public void setNotificacao(String notificacao) {
		this.notificacao = notificacao;
	}

	public boolean isNotificacaoLida() {
		return notificacaoLida;
	}

	public void setNotificacaoLida(boolean notificacaoLida) {
		this.notificacaoLida = notificacaoLida;
	}

}
