package br.com.fdw.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "disciplina_id", sequenceName = "disciplina_seq")
public class Disciplina extends AbstractEntity implements Observer {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	private String nome;
	private String area;

	@OneToMany(mappedBy = "disciplina")
	private List<Turma> turmas;

	@ManyToOne(fetch = FetchType.EAGER)
	private Curso curso;

	// Observer
	private String notificacao;
	private boolean notificacaoLida;

	// Getters & Setters

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public List<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNotificacao() {
		return notificacao;
	}

	public void setNotificacao(String notificacao) {
		this.notificacao = notificacao;
	}

	public boolean isNotificacaoLida() {
		return notificacaoLida;
	}

	public void setNotificacaoLida(boolean notificacaoLida) {
		this.notificacaoLida = notificacaoLida;
	}
	
	

}
