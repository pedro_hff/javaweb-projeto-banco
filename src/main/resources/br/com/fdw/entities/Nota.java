package br.com.fdw.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="nota_id",sequenceName="nota_seq")
public class Nota extends AbstractEntity{

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Long id;
	private String unidade;
	private Double nota;
	private Double peso;
	
	@ManyToOne
	private Desempenho desempenho;
	
	public Nota(){}
	//Getters & Setters
	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	public String getUnidade() {
		return unidade;
	}
	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}
	public Double getNota() {
		return nota;
	}
	public void setNota(Double nota) {
		this.nota = nota;
	}
	public Double getPeso() {
		return peso;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public Desempenho getDesempenho() {
		return desempenho;
	}
	public void setDesempenho(Desempenho desempenho) {
		this.desempenho = desempenho;
	}
	

}
