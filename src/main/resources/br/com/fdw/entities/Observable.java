package br.com.fdw.entities;

import java.util.List;

import javax.persistence.Transient;

public abstract class Observable extends AbstractEntity{
	
	@Transient
	private List<Observer> observers;
	
	@Transient
	static final String notifStart = "A turma ";
	
	@Transient
	static final String closeToLimit = " está chegando perto do limite de alunos.";
	
	@Transient
	static final String maxLimit = " está com o limite máximo de alunos.";
	
	@Transient
	static final int limit = 25;
	
	public void verificaLimiteAlunos(int num, long id){
		if((num > (limit - 5)) && (num < limit)){
			notificaObservers(id, closeToLimit);
		}else{
			if(num >= limit)
				notificaObservers(id, maxLimit);
		}
	}

	public void notificaObservers(Long id, String msg){
		String idString = Long.toString(id);
		for (Observer observer : observers) {
			try{
				observer.setNotificacaoLida(false);
				observer.setNotificacao(notifStart + idString + msg);				
			}catch (Exception e) {
				System.out.println("erro na notificacao");
			}
		}
	}
	
	public void addObserver(Observer observer){
		observers.add(observer);
	}

	public List<Observer> getObservers() {
		return observers;
	}

	public void setObservers(List<Observer> observers) {
		this.observers = observers;
	}
	
	
	
}
