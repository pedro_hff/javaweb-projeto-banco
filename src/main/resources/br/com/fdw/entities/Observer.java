package br.com.fdw.entities;

public interface Observer {


	public String getNotificacao();
	// {
	// return notificacao;
	// }

	public void setNotificacao(String notificacao);
	// {
	// this.notificacao = notificacao;
	// }

	public boolean isNotificacaoLida();
	// {
	// return notificacaoLida;
	// }

	public void setNotificacaoLida(boolean notificacaoLida);
	// {
	// this.notificacaoLida = notificacaoLida;
	// }
}
