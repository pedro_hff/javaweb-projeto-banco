package br.com.fdw.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="turma_id",sequenceName="turma_seq")
public class Turma extends Observable {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Long id;
	private Integer semestre;
	private Integer ano;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Professor professor;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Disciplina disciplina;
	
	@OneToMany(mappedBy="turma")
	private List<Desempenho> desempenhos;
	
	public Turma(){}
	

	public void verificaLimiteAlunos() {
		super.verificaLimiteAlunos(this.getObservers().size(), this.id);
	}
	
	//Getters & Setters
	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getSemestre() {
		return semestre;
	}
	public void setSemestre(Integer semestre) {
		this.semestre = semestre;
	}
	public Integer getAno() {
		return ano;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public Professor getProfessor() {
		return professor;
	}
	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
	public Disciplina getDisciplina() {
		return disciplina;
	}
	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}
	public List<Desempenho> getDesempenhos() {
		return desempenhos;
	}
	public void setDesempenhos(List<Desempenho> desempenhos) {
		this.desempenhos = desempenhos;
	}
	
	
	
	

}
