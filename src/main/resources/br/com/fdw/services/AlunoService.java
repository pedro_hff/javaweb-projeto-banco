package br.com.fdw.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fdw.dao.AlunoDAO;
import br.com.fdw.dao.DesempenhoDAO;
import br.com.fdw.entities.Aluno;
import br.com.fdw.entities.Desempenho;
import br.com.fdw.entities.Nota;

@Service
@Transactional
public class AlunoService {

	@Autowired
	private AlunoDAO daoAluno;

	@Autowired
	private DesempenhoDAO daoDesempenho;

	// Apagar?
	public void CadastraAluno(Aluno aluno) {
		daoAluno.inserir(aluno);
	}

	// apagar?
	public Aluno buscarAlunoPorId(Long id) {
		return daoAluno.buscarPorId(id);
	}

	public List<Aluno> listarAprovados(long turmaID) {
		List<Aluno> alunos = new ArrayList<>();

		List<Desempenho> desempenhos = daoDesempenho.listarDesempenhoDaTurma(turmaID);

		for (Desempenho desempenho : desempenhos) {
			for (Nota nota : desempenho.getNotas()) {
				if (nota.getNota() >= 7.0) {
					if (!alunos.contains(desempenho.getAluno()))
						alunos.add(desempenho.getAluno());
				}
			}
		}

		return alunos;
	}

	public List<Aluno> listarReprovados(long turmaID) {
		List<Aluno> alunos = new ArrayList<>();

		List<Desempenho> desempenhos = daoDesempenho.listarDesempenhoDaTurma(turmaID);

		for (Desempenho desempenho : desempenhos) {
			for (Nota nota : desempenho.getNotas()) {
				if (nota.getNota() < 7.0) {
					if (!alunos.contains(desempenho.getAluno()))
						alunos.add(desempenho.getAluno());
				}
			}
		}

		return alunos;
	}

	public List<Aluno> listarTodos() {
		return daoAluno.listar();
	}

	public void atualizar(Aluno aluno) {
		daoAluno.atualizar(aluno);
	}

	public List<Aluno> buscaPorNome(String nome) {
		return daoAluno.buscaPorNome(nome);
	}

}
