package br.com.fdw.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fdw.dao.AlunoDAO;
import br.com.fdw.dao.CursoDAO;
import br.com.fdw.dao.DisciplinaDAO;
import br.com.fdw.dao.ProfessorDAO;
import br.com.fdw.entities.Aluno;
import br.com.fdw.entities.Curso;
import br.com.fdw.entities.Disciplina;
import br.com.fdw.entities.Professor;

@Service
@Transactional
public class CursoService {

	@Autowired
	private CursoDAO daoCurso;

	@Autowired
	private AlunoDAO daoAluno;

	@Autowired
	private ProfessorDAO daoProfessor;

	@Autowired
	private DisciplinaDAO daoDisciplina;

	// deletar
	public void CadastraCurso(Curso curso) {
		daoCurso.inserir(curso);
	}

	public void atualizar(Curso curso) {
		daoCurso.atualizar(curso);
	}

	// deletar
	public Curso buscarCursoPorId(Long id) {
		return daoCurso.buscarPorId(id);
	}

	public List<Curso> listar() {
		return daoCurso.listar();
	}

	public void matriculaAluno(Long cursoID, Long alunoID) {

		Aluno aluno = daoAluno.buscarPorId(alunoID);
		Curso curso = daoCurso.buscarPorId(cursoID);

		aluno.setCurso(curso);

		// Adicionar aluno ao curso
		List<Aluno> alunos = curso.getAlunos();
		alunos.add(aluno);
		curso.setAlunos(alunos);

		daoAluno.atualizar(aluno);
		daoCurso.atualizar(curso);
	}

	public void acrescentaProfessor(Long cursoID, Long profID) {

		Curso curso = daoCurso.buscarPorId(cursoID);
		Professor professor = daoProfessor.buscarPorId(profID);

		// Adiciona curso ao professor
		List<Curso> cursos = professor.getCursos();
		cursos.add(curso);
		professor.setCursos(cursos);

		// Adiciona professor ao curso
		List<Professor> professores = curso.getProfessores();
		professores.add(professor);
		curso.setProfessores(professores);

		daoCurso.atualizar(curso);
		daoProfessor.atualizar(professor);
	}

	public void acrescentaDisciplina(Long cursoID, Long disciplinaID) {

		Curso curso = daoCurso.buscarPorId(cursoID);
		Disciplina disciplina = daoDisciplina.buscarPorId(disciplinaID);

		System.out.println(disciplina.getArea() + disciplina.getNome());

		// Adiciona curso a disciplina
		disciplina.setCurso(curso);

		// Adiciona professor ao curso
		List<Disciplina> disciplinas = curso.getDisciplinas();
		disciplinas.add(disciplina);
		curso.setDisciplinas(disciplinas);

		daoCurso.atualizar(curso);
		daoDisciplina.atualizar(disciplina);
	}

	public String excluir(Long id) {
		Curso curso = daoCurso.buscarPorId(id);
		String s = curso.getNome();
		daoCurso.excluir(curso);
		return s;
	}

	public List<Curso> buscaPorNome(String nome) {
		return daoCurso.buscaPorNome(nome);
	}

}