package br.com.fdw.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fdw.dao.DesempenhoDAO;
import br.com.fdw.entities.Aluno;
import br.com.fdw.entities.Desempenho;
import br.com.fdw.entities.Turma;

@Service
@Transactional
public class DesempenhoService {
	
	@Autowired
	private DesempenhoDAO daoDesempenho;

	// deletar?
	public void CadastraDesempenho(Desempenho desempenho) {
		daoDesempenho.inserir(desempenho);
	}

	// deletar?
	public Desempenho buscarDesempenhoPorId(Long id) {
		return daoDesempenho.buscarPorId(id);
	}

	public List<Turma> listaTurmasdoAluno(Aluno aluno){
		return daoDesempenho.listaTurmasdoAluno(aluno);
	}
}
