package br.com.fdw.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fdw.dao.DisciplinaDAO;
import br.com.fdw.dao.TurmaDAO;
import br.com.fdw.entities.Disciplina;
import br.com.fdw.entities.Turma;

@Service
@Transactional
public class DisciplinaService {
	
	@Autowired
	DisciplinaDAO dao;
	
	@Autowired
	TurmaDAO daoTurma;

	public List<Disciplina> listar() {
		return dao.listar();
	}

	public Long CadastrarDisciplina(Disciplina disciplina) {
		Long id = null;
		if(disciplina.getId()==null){
			dao.inserir(disciplina);
			id = disciplina.getId();			
		}else{
			dao.atualizar(disciplina);
			id = disciplina.getId();
		}
		return id;

	}

	public Disciplina buscarDisciplinaPorId(Long id) {
		return dao.buscarPorId(id);
	}

	public void cadastraTurma(Long disciplinaID, Long turmaID) {
		Disciplina d = dao.buscarPorId(disciplinaID);
		Turma t = daoTurma.buscarPorId(turmaID);

		List<Turma> turmas = new ArrayList<Turma>();

		turmas.add(t);

		d.setTurmas(turmas);

		t.setDisciplina(d);

		dao.atualizar(d);
		daoTurma.atualizar(t);
	}
	
	public String excluir(Long id){
		Disciplina d = dao.buscarPorId(id);
		String nome = d.getNome();
		dao.excluir(d);
		return nome;
	}
	
	public List<Disciplina> buscaPorNome(String nome){
		return dao.buscarPorNome(nome);
	}

}
