package br.com.fdw.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fdw.dao.EnderecoDAO;
import br.com.fdw.entities.Endereco;

@Service
@Transactional
public class EnderecoService {

	public static void CadastraEndereco(Endereco endereco) {
		EnderecoDAO dao = new EnderecoDAO();
		dao.inserir(endereco);
	}

	public static Endereco buscarEnderecoPorId(Long id) {
		EnderecoDAO dao = new EnderecoDAO();
		return dao.buscarPorId(id);
	}

}
