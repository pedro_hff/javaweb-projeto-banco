package br.com.fdw.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fdw.dao.DesempenhoDAO;
import br.com.fdw.dao.NotaDAO;
import br.com.fdw.entities.Desempenho;
import br.com.fdw.entities.Nota;

@Service
@Transactional
public class NotaService {

	// apagar
	public static void CadastraNota(Nota nota) {
		NotaDAO dao = new NotaDAO();
		dao.inserir(nota);
	}

	// apagar
	public static Nota buscarNotaPorId(Long id) {
		NotaDAO dao = new NotaDAO();
		return dao.buscarPorId(id);
	}

	public static void assinalarNota(Long alunoID, Long turmaID, Nota nota) {
		DesempenhoDAO dDAO = new DesempenhoDAO();
		NotaDAO nDAO = new NotaDAO();

		Desempenho desempenho = dDAO.buscarPorAlunoeTurma(alunoID, turmaID);
		List<Nota> notas = desempenho.getNotas();
		notas.add(nota);
		desempenho.setNotas(notas);

		nDAO.inserir(nota);
		dDAO.atualizar(desempenho);
	}

}
