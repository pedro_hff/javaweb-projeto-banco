package br.com.fdw.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fdw.dao.ProfessorDAO;
import br.com.fdw.entities.Professor;

@Service
@Transactional
public class ProfessorService {

	@Autowired
	private ProfessorDAO daoProfessor;

	public void CadastraProfessor(Professor professor) {
		daoProfessor.inserir(professor);
	}

	public Professor buscarProfessorPorId(Long id) {
		return daoProfessor.buscarPorId(id);
	}

	public List<Professor> listarTodos() {
		return daoProfessor.listar();
	}

	public void atualizar(Professor professor) {
		daoProfessor.atualizar(professor);
	}

	public List<Professor> buscaPorNome(String nome) {
		return daoProfessor.buscaPorNome(nome);
	}

	public String excluir(Long id) {
		Professor professor = buscarProfessorPorId(id);
		String nome = professor.getNome();
		daoProfessor.excluir(professor);

		return nome;
	}
}
