package br.com.fdw.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fdw.dao.AlunoDAO;
import br.com.fdw.dao.CursoDAO;
import br.com.fdw.dao.DesempenhoDAO;
import br.com.fdw.dao.DisciplinaDAO;
import br.com.fdw.dao.ProfessorDAO;
import br.com.fdw.dao.TurmaDAO;
import br.com.fdw.entities.Aluno;
import br.com.fdw.entities.Curso;
import br.com.fdw.entities.Desempenho;
import br.com.fdw.entities.Disciplina;
import br.com.fdw.entities.Observer;
import br.com.fdw.entities.Professor;
import br.com.fdw.entities.Turma;

@Service
@Transactional
public class TurmaService {

	@Autowired
	private TurmaDAO daoTurma;

	@Autowired
	private ProfessorDAO daoProfessor;

	@Autowired
	private DisciplinaDAO daoDisciplina;

	@Autowired
	private AlunoDAO daoAluno;

	@Autowired
	private CursoDAO daoCurso;

	@Autowired
	private DesempenhoDAO daoDesempenho;

	// apagar?
	public Long CadastraTurma(Turma turma) {
		daoTurma.inserir(turma);

		return turma.getId();
	}

	public void atualizarTurma(Turma turma) {
		daoTurma.atualizar(turma);
	}

	// apagar?
	public Turma buscarTurmaPorId(Long id) {
		return daoTurma.buscarPorId(id);
	}

	public List<Turma> listar() {
		return daoTurma.listar();
	}

	public void criarTurma(Long disciplinaDAO, Long professorID, Turma turma) {
		Professor professor = daoProfessor.buscarPorId(professorID);
		Disciplina disciplina = daoDisciplina.buscarPorId(disciplinaDAO);

		// Turma
		turma.setDisciplina(disciplina);
		turma.setProfessor(professor);
		daoTurma.inserir(turma);

		// Professor
		List<Turma> turmas = professor.getTurmas();
		turmas.add(turma);
		professor.setTurmas(turmas);

		// Disciplina
		List<Turma> dturmas = disciplina.getTurmas();
		dturmas.add(turma);
		disciplina.setTurmas(dturmas);

		daoTurma.atualizar(turma);
		daoProfessor.atualizar(professor);
		daoDisciplina.atualizar(disciplina);
	}

	public List<Observer> getAlunosMatriculados(Long turma) {
		return daoAluno.listarAlunosDaTurmaObserver(turma);
	}

	public void verificarLimite(Turma turma) {
		List<Observer> observers;
		// Observer
		int qtdAlunos = daoDesempenho.quantidadeAlunosNaTurma(turma.getId());
		if (qtdAlunos > 20) {
			observers = getAlunosMatriculados(turma.getId());
			observers.add(turma.getProfessor());
			observers.add(turma.getDisciplina());
			observers.add(turma.getDisciplina().getCurso());

			System.out.println("Teste - " + Integer.toString(observers.size()));
			turma.setObservers(observers);
			turma.verificaLimiteAlunos();

			// atualizar entidades observers no banco
			for (Observer observer : turma.getObservers()) {
				try {
					if (observer.getClass().equals(Professor.class)) {
						System.out.println("Professor atualizado");
						daoProfessor.atualizar((Professor) observer);
					}
					if (observer.getClass().equals(Aluno.class)) {
						System.out.println("Aluno atualizado");
						daoAluno.atualizar((Aluno) observer);
					}
					if (observer.getClass().equals(Disciplina.class)) {
						System.out.println("Disciplina atualizada");
						daoDisciplina.atualizar((Disciplina) observer);
					}
					if (observer.getClass().equals(Curso.class)) {
						System.out.println("Curso atualizado");
						daoCurso.atualizar((Curso) observer);
					}

				} catch (Exception e) {
					if (observer == null)
						System.out.println("Observer nulo " + observers.indexOf(observer));
					else
						System.out.println("Não pode atualizar " + observer.getClass().getName());
				}
			}
		}
	}

	public void matricularAluno(Long alunoID, Long turmaID) {
		// Aluno
		Aluno aluno = daoAluno.buscarPorId(alunoID);

		// Desempenho
		Desempenho desempenho = new Desempenho();
		desempenho.setAluno(aluno);

		// Turma
		Turma turma = daoTurma.buscarPorId(turmaID);
		List<Desempenho> desempenhos = turma.getDesempenhos();
		desempenhos.add(desempenho);
		turma.setDesempenhos(desempenhos);

		// Continuacao aluno
		List<Desempenho> adesempenhos = aluno.getDesempenhos();
		adesempenhos.add(desempenho);
		aluno.setDesempenhos(adesempenhos);

		// Continuacao desempenho
		desempenho.setTurma(turma);

		verificarLimite(turma);
		daoDesempenho.inserir(desempenho);
		daoAluno.atualizar(aluno);
		daoTurma.atualizar(turma);
	}

	public void matricularProfessor(Long professorID, Long turmaID) {
		Professor prof = daoProfessor.buscarPorId(professorID);
		Turma turma = daoTurma.buscarPorId(turmaID);
		turma.setProfessor(prof);
		List<Curso> cursosProf = prof.getCursos();
		if (!cursosProf.contains(turma.getDisciplina().getCurso())) {
			cursosProf.add(turma.getDisciplina().getCurso());
			prof.setCursos(cursosProf);
			daoProfessor.atualizar(prof);
		}
		daoTurma.atualizar(turma);

	}

	public void excluir(Long id) {
		Turma t = daoTurma.buscarPorId(id);
		daoTurma.excluir(t);
	}

	public List<Turma> buscaPorNome(Long id) {
		return daoTurma.buscaPorCurso(daoCurso.buscarPorId(id));
	}
}
