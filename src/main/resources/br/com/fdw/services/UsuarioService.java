package br.com.fdw.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fdw.dao.UsuarioDAO;
import br.com.fdw.entities.Usuario;
import br.com.fdw.utils.Utils;

@Service
@Transactional
public class UsuarioService {

	@Autowired
	private UsuarioDAO daoUsuario;

	// deletar?
	public void cadastraUsuario(Usuario usuario) {
		daoUsuario.inserir(usuario);
	}

	// deletar?
	public Usuario buscarUsuarioPorId(Long id) {
		return daoUsuario.buscarPorId(id);
	}

	public void marcarNotificacaoLida(Usuario usuario) {
		usuario.setNotificacao("");
		usuario.setNotificacaoLida(true);
		daoUsuario.atualizar(usuario);
	}

	public Usuario autenticar(String login, String senha, RedirectAttributes ra) {
		Usuario user = daoUsuario.findByLogin(login);
		try {
			if (!user.getLogin().isEmpty() && user != null) {
				if (user.getSenha().equals(Utils.MD5(senha))) {
					ra.addFlashAttribute("msgErro", String.format("Usuario %s logado com sucesso.", user.getNome()));
					return user;
				} else {
					ra.addFlashAttribute("msgErro", "Senha incorreta.");
					return null;
				}
			} else {
				ra.addFlashAttribute("msgErro", "Usuario não encontrado.");
				return null;
			}
		} catch (NullPointerException e) {
			ra.addFlashAttribute("msgErro", "Usuario não encontrado.");
			return null;
		}
	}

}
