package br.com.fdw.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import br.com.fdw.entities.Observer;
import br.com.fdw.entities.Usuario;



public class Interceptor extends HandlerInterceptorAdapter{
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		HttpSession session = request.getSession();
		String uri = request.getRequestURI();		
		if(session.getAttribute("usuario") == null){
			if(!uri.endsWith(".js") && !uri.endsWith(".css") && !uri.endsWith(".png")){
				if(!uri.equals("/") && !uri.equals(request.getContextPath() + "/") && !uri.equals(request.getContextPath() + "/logar") ){
					System.out.println("Acesso bloqueado: " + uri);
					response.sendRedirect(request.getContextPath());
					return false;
				}else
					return true;
			}
		}else{
			Usuario usuario = (Usuario) session.getAttribute("usuario");
			System.out.println("Usuario: "+usuario.getLogin()+ " // Acesso: " + uri);
			
			if(!request.getRequestURI().endsWith("marcarcomolida"))
				session.setAttribute("pagAtual", request.getRequestURI());
			
			return true;	
		}
		
		return true;
	}
	
	public void verificaNotificacao(ModelAndView mv, Observer obs) {
//		System.out.println(Boolean.toString(obs.isNotificacaoLida()) + " - "
//				+ Boolean.toString(obs.getNotificacao() != null ) + " - "
//				+ obs.getNotificacao()
//				);
		if (!obs.isNotificacaoLida() && obs.getNotificacao() != null && !obs.getNotificacao().isEmpty()) {
			System.out.println("iago viadin");
			mv.getModelMap().addAttribute("msgNotif", obs.getNotificacao());
		}
	}
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		HttpSession session = request.getSession();
		if (!session.isNew() && session.getAttribute("usuario") != null) {
			Usuario usuario = (Usuario) session.getAttribute("usuario");
			verificaNotificacao(modelAndView, usuario);
		}
	}
}
