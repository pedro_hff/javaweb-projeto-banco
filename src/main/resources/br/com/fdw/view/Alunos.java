package br.com.fdw.view;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fdw.entities.Aluno;
import br.com.fdw.entities.Turma;
import br.com.fdw.services.AlunoService;
import br.com.fdw.services.TurmaService;
import br.com.fdw.utils.Utils;

@Controller
@RequestMapping("/alunos")
public class Alunos {

	@Autowired
	private AlunoService alunoService;

	@Autowired
	private TurmaService turmaService;

	@RequestMapping(value = { "/listar", "" }, method = RequestMethod.GET)
	public String listar(ModelMap map, @ModelAttribute("msgInfo") String msgInfo,
			@ModelAttribute("msgSucesso") String msgSucesso, 
			@ModelAttribute("msgErro") String msgErro, @RequestParam(value="q", required=false)String nomeBusca) {
		List<Aluno> alunos;
		if(nomeBusca == null)
			alunos = alunoService.listarTodos();
		else
			alunos = alunoService.buscaPorNome(nomeBusca);
		map.addAttribute("alunos", alunos);

		if (!msgInfo.isEmpty())
			map.addAttribute("msgInfo", msgInfo);
		if (!msgErro.isEmpty())
			map.addAttribute("msgErro", msgErro);
		if (!msgSucesso.isEmpty())
			map.addAttribute("msgSucesso", msgSucesso);

		return "/paginas/alunos";
	}

	@RequestMapping(value = "/cadastro", method = RequestMethod.GET)
	public String paginaCadastro(ModelMap map) {
		map.addAttribute("aluno", new Aluno());

		return "/paginas/alunocadastro";
	}
	
	@RequestMapping(value = "/editar/{id}", method = RequestMethod.GET)
	public String editar(ModelMap map, HttpSession session, @PathVariable("id")Long id){
		Aluno aluno = alunoService.buscarAlunoPorId(id);
		map.addAttribute("alunoEditar", aluno);
		session.setAttribute("alunoEditar", aluno);		
		return "paginas/alunocadastro";
	}

	@RequestMapping(value = "/cadastro", method = RequestMethod.POST)
	public String cadastra(ModelMap map, @ModelAttribute("aluno") Aluno aluno, HttpSession session) {
		Aluno alunoEdicao = (Aluno)session.getAttribute("alunoEditar");
		if(alunoEdicao == null){
			aluno.setSenha(Utils.MD5(aluno.getSenha()));
			alunoService.CadastraAluno(aluno);
		}else{
			alunoEdicao.setNome(aluno.getNome());
			alunoEdicao.setCpf(aluno.getCpf());
			alunoEdicao.setRg(aluno.getRg());
			alunoEdicao.setLogin(aluno.getLogin());
			if(aluno.getSenha()!= null && !aluno.getSenha().isEmpty())
				alunoEdicao.setSenha(Utils.MD5(aluno.getSenha()));
			session.removeAttribute("alunoEditar");
			alunoService.atualizar(alunoEdicao);
		}
		return "redirect:/alunos/";
	}

	@RequestMapping(value = "/matricular/{id}", method = RequestMethod.GET)
	public String paginaMatricula(ModelMap map, @PathVariable("id") Long id, HttpSession session) {
		Aluno aluno = alunoService.buscarAlunoPorId(id);
		List<Turma> turmas = turmaService.listar();
		map.addAttribute("aluno", aluno);
		map.addAttribute("turmas", turmas);
		session.setAttribute("alunoMatricula", aluno);

		return "/paginas/matriculaaluno";
	}

	@RequestMapping(value = "/matricular", method = RequestMethod.POST)
	public String matricular(ModelMap map, HttpSession session, @RequestParam("turmaid") Long turmaid, RedirectAttributes ra){
		Aluno aluno = (Aluno) session.getAttribute("alunoMatricula");
		turmaService.matricularAluno(aluno.getId(), turmaid);
		session.removeAttribute("alunoMatricula");
		ra.addFlashAttribute("msgSucesso", "Aluno " + aluno.getNome() + " matriculado na turma" + turmaid  +".");
		return "redirect:/alunos/";
	}

}
