package br.com.fdw.view;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fdw.entities.Curso;
import br.com.fdw.services.CursoService;

@Controller
@RequestMapping("/cursos")
public class Cursos {

	@Autowired
	private CursoService cursoService;

	@RequestMapping(value = { "/listar", "" }, method = RequestMethod.GET)
	public String listar(ModelMap map, @ModelAttribute("msgInfo") String msgInfo,
			@ModelAttribute("msgSucesso") String msgSucesso, 
			@ModelAttribute("msgErro") String msgErro, @RequestParam(value = "q", required = false)String nomeBusca) {
		List<Curso> cursos;
		if(nomeBusca == null)
			cursos = cursoService.listar();
		else
			cursos = cursoService.buscaPorNome(nomeBusca);
		map.addAttribute("cursos", cursos);
		if (!msgInfo.isEmpty()) {
			map.addAttribute("msgInfo", msgInfo);
			System.out.println(msgInfo);
		}
		if (!msgErro.isEmpty())
			map.addAttribute("msgErro", msgErro);
		if (!msgSucesso.isEmpty())
			map.addAttribute("msgSucesso", msgSucesso);
		return "/paginas/cursos";
	}

	@RequestMapping(value = "/cadastro", method = RequestMethod.GET)
	public String paginaCadastro(ModelMap map) {
		Curso curso = new Curso();
		map.addAttribute("cursoCadastro", curso);
		return "paginas/cursocadastro";
	}

	@RequestMapping(value = "/cadastro", method = RequestMethod.POST)
	public String cadastrarCurso(ModelMap map, @ModelAttribute("cursoCadastro") Curso curso, RedirectAttributes ra) {
		cursoService.CadastraCurso(curso);
		ra.addFlashAttribute("msgSucesso", "Curso " + curso.getNome() + " cadastrado no sistema.");
		return "redirect:/cursos/";
	}

	@RequestMapping(value = "/editar/{id}", method = RequestMethod.GET)
	public String paginaEditar(ModelMap map, @PathVariable Long id, HttpSession session) {
		Curso curso = cursoService.buscarCursoPorId(id);
		map.addAttribute("cursoEditar", curso);
		session.setAttribute("cursoEdicao", curso);
		return "paginas/cursoeditar";
	}

	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	public String editarCurso(ModelMap map, @ModelAttribute("cursoEditar") Curso curso, RedirectAttributes ra,
			HttpSession session) {
		Curso cursoAntigo = (Curso) session.getAttribute("cursoEdicao");
		cursoAntigo.setNome(curso.getNome());
		cursoAntigo.setTipo(curso.getTipo());
		cursoService.atualizar(cursoAntigo);
		ra.addFlashAttribute("msgInfo", "Curso " + cursoAntigo.getNome() + " editado.");
		return "redirect:/cursos/";
	}

	@RequestMapping(value = "/excluir/{id}", method = RequestMethod.GET)
	public String excluirCurso(ModelMap map, @PathVariable("id") Long id, RedirectAttributes ra) {
		String nome = cursoService.excluir(id);
		ra.addFlashAttribute("msgSucesso", "Curso " + nome + " excluido.");
		return "redirect:/cursos/";
	}

}
