package br.com.fdw.view;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fdw.entities.Disciplina;
import br.com.fdw.services.CursoService;
import br.com.fdw.services.DisciplinaService;

@Controller
@RequestMapping("/disciplinas")
public class Disciplinas {

	@Autowired
	DisciplinaService dService;

	@Autowired
	CursoService cService;

	@RequestMapping(value = { "/listar", "" }, method = RequestMethod.GET)
	public String listar(ModelMap map, @ModelAttribute("msgInfo") String msgInfo,
			@ModelAttribute("msgSucesso") String msgSucesso, 
			@ModelAttribute("msgErro") String msgErro, @RequestParam(value="q", required=false)String nomeBusca) {
		List<Disciplina> disciplinas;
		if(nomeBusca == null)
			disciplinas = dService.listar();
		else
			disciplinas = dService.buscaPorNome(nomeBusca);
		map.addAttribute("disciplinas", disciplinas);

		if (!msgInfo.isEmpty()) {
			map.addAttribute("msgInfo", msgInfo);
		}
		if (!msgErro.isEmpty())
			map.addAttribute("msgErro", msgErro);
		if (!msgSucesso.isEmpty())
			map.addAttribute("msgSucesso", msgSucesso);

		return "/paginas/disciplinas";
	}

	@RequestMapping(value = "/cadastro", method = RequestMethod.GET)
	public String paginaCadastro(ModelMap map) {

		map.addAttribute("cursos", cService.listar());
		map.addAttribute("disciplinaCadastro", new Disciplina());
		return "/paginas/disciplinascadastro";
	}

	@RequestMapping(value = "/cadastro", method = RequestMethod.POST)
	public String cadastro(ModelMap map, RedirectAttributes ra,
			@ModelAttribute("disciplinaCadastro") Disciplina disciplina, 
			@RequestParam("cursoid") String cursoid, HttpSession session) {
		Disciplina disciplinaAntigo = (Disciplina) session.getAttribute("disciplinaEdicao");
		if(disciplinaAntigo == null){	
			Long idDisc = dService.CadastrarDisciplina(disciplina);
			cService.acrescentaDisciplina(Long.valueOf(cursoid), idDisc);
			ra.addFlashAttribute("msgSucesso", "Disciplina " + disciplina.getNome() + " cadastrada no sistema.");
		}else{
			disciplinaAntigo.setNome(disciplina.getNome());
			disciplinaAntigo.setArea(disciplina.getArea());
			dService.CadastrarDisciplina(disciplinaAntigo);
			ra.addFlashAttribute("msgInfo", "Disciplina " + disciplinaAntigo.getNome() + " editada.");
			session.removeAttribute("disciplinaEdicao");
		}

		return "redirect:/disciplinas/";
	}

	@RequestMapping(value = "/editar/{id}", method = RequestMethod.GET)
	public String editar(ModelMap map, RedirectAttributes ra, @PathVariable("id") Long id, HttpSession session) {
		Disciplina disc = dService.buscarDisciplinaPorId(id);
		map.addAttribute("disciplinaCadastro", disc);
		map.addAttribute("cursos", cService.listar());
		session.setAttribute("disciplinaEdicao", disc);
		return "paginas/disciplinascadastro";
	}


	@RequestMapping(value = "/excluir/{id}", method = RequestMethod.GET)
	public String excluirDisciplina(ModelMap map, @PathVariable("id") Long id, RedirectAttributes ra) {
		String nome = dService.excluir(id);
		ra.addFlashAttribute("msgSucesso", "Disciplina " + nome + " excluida.");
		return "redirect:/disciplinas/";
	}

}
