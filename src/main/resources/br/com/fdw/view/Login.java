package br.com.fdw.view;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fdw.entities.Aluno;
import br.com.fdw.entities.Professor;
import br.com.fdw.entities.Usuario;
import br.com.fdw.services.UsuarioService;

@Controller
public class Login {

	@Autowired
	private UsuarioService usuarioService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(ModelMap map, HttpSession session, @ModelAttribute("msgInfo") String msgInfo,
			@ModelAttribute("msgSucesso") String msgSucesso, @ModelAttribute("msgErro") String msgErro) {
		Usuario usuarioSession = (Usuario) session.getAttribute("usuario");
		if (usuarioSession == null) {
			if (!msgInfo.isEmpty())
				map.addAttribute("msgInfo", msgInfo);
			if (!msgErro.isEmpty())
				map.addAttribute("msgErro", msgErro);
			if (!msgSucesso.isEmpty())
				map.addAttribute("msgSucesso", msgSucesso);

			Usuario usuario = new Usuario();
			map.addAttribute("usuario", usuario);
			return "paginas/index";
		} else {
			return "redirect:/cursos";
		}
	}

	@RequestMapping(value = "/logar", method = RequestMethod.POST)
	public String logar(ModelMap map, @ModelAttribute("usuario") Usuario usuario, HttpSession session,
			RedirectAttributes ra) {
		if (usuario.getLogin().equals("admin")) {
			if (usuario.getSenha().equals("admin")) {
				usuario.setNome("Administrador");
				session.setAttribute("usuario", usuario);
			} else {
				ra.addFlashAttribute("msgErro", "Senha do admin incorreta");
				return "redirect:/";
			}
		} else {
			usuario = usuarioService.autenticar(usuario.getLogin(), usuario.getSenha(), ra);
			if (usuario != null) {
				session.setAttribute("usuario", usuario);
				System.out.println("Usuario logado - " + usuario.getNome());
			} else {
				return "redirect:/";
			}
		}
		int tipo = 0;
		if (usuario instanceof Professor) {
			tipo = 1;
			session.setAttribute("tipo", tipo);

		}
		if (usuario instanceof Aluno) {
			tipo = 2;
			session.setAttribute("tipo", tipo);
			return "redirect:/turmas/listar";
		}
		session.setAttribute("tipo", tipo);
		return "redirect:/cursos/listar";
	}

	@RequestMapping(value = "/marcarcomolida", method = RequestMethod.GET)
	public String removerMsg(HttpSession session, HttpServletRequest req) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		usuarioService.marcarNotificacaoLida(usuario);
		String pagina = (String) session.getAttribute("pagAtual");
		pagina = pagina.substring(1).substring(pagina.substring(1).indexOf("/"));
		return "redirect:" + pagina;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
		session.invalidate();

		return "redirect:/";
	}
}
