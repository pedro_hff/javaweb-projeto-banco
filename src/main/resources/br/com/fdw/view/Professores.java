package br.com.fdw.view;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fdw.entities.Professor;
import br.com.fdw.entities.Turma;
import br.com.fdw.services.ProfessorService;
import br.com.fdw.services.TurmaService;
import br.com.fdw.utils.Utils;

@Controller
@RequestMapping("/professores")
public class Professores {

	@Autowired
	private ProfessorService serviceProf;

	@Autowired
	private TurmaService serviceTurma;

	@RequestMapping(value = { "/listar", "" }, method = RequestMethod.GET)
	public String paginaListar(ModelMap map, @ModelAttribute("msgInfo") String msgInfo,
			@ModelAttribute("msgSucesso") String msgSucesso, @ModelAttribute("msgErro") String msgErro,
			@RequestParam(value = "q", required = false) String nomeBusca) {
		List<Professor> professores;
		if (nomeBusca == null) {
			professores = serviceProf.listarTodos();
		} else {
			professores = serviceProf.buscaPorNome(nomeBusca);
		}
		map.addAttribute("professores", professores);

		// msgs alertas
		if (!msgInfo.isEmpty())
			map.addAttribute("msgInfo", msgInfo);
		if (!msgErro.isEmpty())
			map.addAttribute("msgErro", msgErro);
		if (!msgSucesso.isEmpty())
			map.addAttribute("msgSucesso", msgSucesso);
		return "/paginas/professores";
	}

	@RequestMapping(value = "/cadastro", method = RequestMethod.GET)
	public String paginaCadastro(ModelMap map) {
		map.addAttribute("profCadastro", new Professor());
		return "/paginas/professorcadastro";
	}

	@RequestMapping(value = "/editar/{id}", method = RequestMethod.GET)
	public String paginaEditar(ModelMap map, HttpSession session, @PathVariable("id") Long id) {
		Professor profEditar = serviceProf.buscarProfessorPorId(id);
		map.addAttribute("profEditar", profEditar);
		session.setAttribute("profEditar", profEditar);
		return "/paginas/professorcadastro";
	}

	@RequestMapping(value = "/cadastro", method = RequestMethod.POST)
	public String cadastro(ModelMap map, HttpSession session, RedirectAttributes ra,
			@ModelAttribute("profCadastro") Professor prof) {
		Professor profEditar = (Professor) session.getAttribute("profEditar");
		if (profEditar == null) {
			prof.setSenha(Utils.MD5(prof.getSenha()));
			serviceProf.CadastraProfessor(prof);
		} else {
			profEditar.setNome(prof.getNome());
			profEditar.setCpf(prof.getCpf());
			profEditar.setRg(prof.getRg());
			profEditar.setLogin(prof.getLogin());
			profEditar.setAreaAtuacao(prof.getAreaAtuacao());
			profEditar.setTitulacao(prof.getTitulacao());
			if (prof.getSenha() != null && !prof.getSenha().isEmpty())
				profEditar.setSenha(Utils.MD5(prof.getSenha()));
			serviceProf.atualizar(profEditar);
			session.removeAttribute("profEditar");
		}
		return "redirect:/professores/";
	}

	@RequestMapping(value = "/matricular/{id}", method = RequestMethod.GET)
	public String paginaMatricula(ModelMap map, @PathVariable("id") Long id, HttpSession session) {
		Professor profMatricula = serviceProf.buscarProfessorPorId(id);
		List<Turma> turmas = serviceTurma.listar();
		map.addAttribute("profMatricula", profMatricula);
		map.addAttribute("turmas", turmas);
		session.setAttribute("profMatricula", profMatricula);
		return "/paginas/professormatricula";
	}

	@RequestMapping(value = "/matricular", method = RequestMethod.POST)
	public String matricular(ModelMap map, HttpSession session, @RequestParam("turmaid") Long turmaid,
			RedirectAttributes ra) {
		Professor professor = (Professor) session.getAttribute("profMatricula");
		serviceTurma.matricularProfessor(professor.getId(), turmaid);

		ra.addFlashAttribute("msgSucesso",
				"Professor " + professor.getNome() + " matriculado na turma" + turmaid + ".");
		session.removeAttribute("profMatricula");
		return "redirect:/professores/";
	}

	@RequestMapping(value = "/excluir/{id}", method = RequestMethod.GET)
	public String excluir(ModelMap map, @PathVariable("id") Long id, RedirectAttributes ra) {
		ra.addFlashAttribute("msgInfo", "Professor " + serviceProf.excluir(id) + " excluido.");
		return "redirect:/professores/";
	}
}