package br.com.fdw.view;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fdw.entities.Aluno;
import br.com.fdw.entities.Disciplina;
import br.com.fdw.entities.Turma;
import br.com.fdw.entities.Usuario;
import br.com.fdw.services.CursoService;
import br.com.fdw.services.DesempenhoService;
import br.com.fdw.services.DisciplinaService;
import br.com.fdw.services.TurmaService;

@Controller
@RequestMapping("/turmas")
public class Turmas {

	@Autowired
	private TurmaService service;

	@Autowired
	private DisciplinaService dService;
	
	@Autowired
	private DesempenhoService desempenhoService;
	
	@Autowired
	private CursoService cursoService;

	@RequestMapping(value = { "/listar", "" }, method = RequestMethod.GET)
	public String listar(ModelMap map, @ModelAttribute("msgInfo") String msgInfo,
			@ModelAttribute("msgSucesso") String msgSucesso, 
			@ModelAttribute("msgErro") String msgErro, HttpSession session, 
			@RequestParam(value="q",required=false)Long cursoBusca) {
		Usuario usuario = (Usuario)session.getAttribute("usuario");
		if(usuario instanceof Aluno){
			List<Turma> turmasAluno = desempenhoService.listaTurmasdoAluno((Aluno)usuario);
			map.addAttribute("turmasAluno", turmasAluno);
		}else{
			List<Turma>turmas;
			if(cursoBusca == null)
				turmas = service.listar();
			else
				turmas = service.buscaPorNome(cursoBusca);
			map.addAttribute("turmas", turmas);
			map.addAttribute("cursosBusca", cursoService.listar());
		}

		if (!msgInfo.isEmpty()) {
			map.addAttribute("msgInfo", msgInfo);
		}
		if (!msgErro.isEmpty())
			map.addAttribute("msgErro", msgErro);
		if (!msgSucesso.isEmpty())
			map.addAttribute("msgSucesso", msgSucesso);

		return "/paginas/turmas";
	}

	@RequestMapping(value = "/cadastro", method = RequestMethod.GET)
	public String paginaCadastro(ModelMap map) {
		List<Disciplina> disciplinas = dService.listar();
		map.addAttribute("disciplinas", disciplinas);
		map.addAttribute("turma", new Turma());
		return "/paginas/turmascadastro";
	}

	@RequestMapping(value = "/cadastro", method = RequestMethod.POST)
	public String cadastro(ModelMap map, RedirectAttributes ra, @ModelAttribute("turma") Turma turma,
			@RequestParam("disciplinaid") Long disciplinaid, HttpSession session) {
		Turma turmaAntigo = (Turma) session.getAttribute("turmaEdicao");
		if(turmaAntigo == null){
			Long idTurma = service.CadastraTurma(turma);
			dService.cadastraTurma(disciplinaid, idTurma);
		}else{
			turmaAntigo.setAno(turma.getAno());
			turmaAntigo.setSemestre(turma.getSemestre());
//			turmaAntigo.setProfessor(professor);
			service.atualizarTurma(turmaAntigo);
			if(turmaAntigo.getDisciplina().getId() != disciplinaid)
				dService.cadastraTurma(disciplinaid, turmaAntigo.getId());
			session.removeAttribute("turmaEdicao");
		}

		return "redirect:/turmas/";
	}

	@RequestMapping(value = "/editar/{id}", method = RequestMethod.GET)
	public String paginaEditar(ModelMap map, @PathVariable Long id, HttpSession session) {
		Turma turma = service.buscarTurmaPorId(id);
		map.addAttribute("turmaEditar", turma);
		map.addAttribute("disciplinas", dService.listar());
		session.setAttribute("turmaEdicao", turma);
		return "paginas/turmascadastro";
	}
	
	@RequestMapping(value = "/excluir/{id}", method = RequestMethod.GET)
	public String excluirCurso(ModelMap map, @PathVariable("id") Long id, RedirectAttributes ra) {
		service.excluir(id);
		ra.addFlashAttribute("msgSucesso", "Turma excluida.");
		return "redirect:/turmas/";
	}

}
