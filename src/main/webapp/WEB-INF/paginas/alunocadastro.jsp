<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/WEB-INF/parts/header.jsp"%>
	<h1>Inser��o/edi��o de aluno</h1>
	
	<form class="form-horizontal" method="post"
		action="${pageContext.request.contextPath}/alunos/cadastro">
	
		<div class="form-group">
			<label for="n_aluno" class="col-xs-2 control-label">Nome:</label>
			<div class="col-xs-4">
				<input type="text" class="form-control" id="n_aluno" name="nome" value="${alunoEditar.nome}" />
			</div>
		</div>
		<div class="form-group">
			<label for="cpf" class="col-xs-2 control-label">CPF:</label>
			<div class="col-xs-4">
				<input type="text" class="form-control" id="cpf" name="cpf" value="${alunoEditar.cpf}" maxlength="11"/>
			</div>	
		</div>
		
		<div class="form-group">
			<label for="rg" class="col-xs-2 control-label">RG:</label>
			<div class="col-xs-4">
				<input type="text" class="form-control" id="rg" name="rg" maxlength="7" value="${alunoEditar.rg}"/>
			</div>	
		</div>
		
		<div class="form-group">
			<label for="login" class="col-xs-2 control-label">Login:</label>
			<div class="col-xs-4">
				<input type="text" class="form-control" id="login" name="login" value="${alunoEditar.login}"/>
			</div>	
		</div>
		
		<div class="form-group">
			<label for="senha" class="col-xs-2 control-label">Senha:</label>
			<div class="col-xs-4">
				<input type="password" class="form-control" id="senha" name="senha" />
			</div>	
		</div>
		
		<div class="form-group">
			<div class="col-xs-6"></div>
			<button type="submit" class="btn btn-default">Salvar</button>
	
		</div>
	</form>
<%@include file="/WEB-INF/parts/footer.jsp"%>