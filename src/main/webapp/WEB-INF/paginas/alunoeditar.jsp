<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/WEB-INF/parts/header.jsp"%>
	<h1>Editar Aluno</h1>
	
	<form class="form-horizontal" method="post"
		action="/FileiDeWanderlyWeb/alunos/editar">
	
		<div class="form-group">
			<label for="n_aluno" class="col-xs-2 control-label">Nome:</label>
			<div class="col-xs-4">
				<input Type="text" class="form-control" id="n_aluno" name="nome"
					value="${aluno.nome}" />
			</div>
		</div>
		<div class="form-group">
			<label for="cpf" class="col-xs-2 control-label">Cpf:</label>
			<div class="col-xs-4">
				<input Type="text" class="form-control" id="cpf" name="cpf"
					value="${aluno.cpf}" />
			</div>
	
		</div>
		<div class="form-group">
			<label for="rg" class="col-xs-2 control-label">RG:</label>
			<div class="col-xs-4">
				<input Type="text" class="form-control" id="rg" name="rg"
					value="${aluno.rg}" />
			</div>
	
		</div>
		<div class="form-group">
			<div class="col-xs-6"></div>
			<button type="submit" class="btn btn-default">Salvar</button>
	
		</div>
	</form>
<%@include file="/WEB-INF/parts/footer.jsp"%>