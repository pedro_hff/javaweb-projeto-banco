<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/WEB-INF/parts/header.jsp"%>

	
	<h1>Listagem de Cursos</h1>
	
	<form class="form-horizontal" method="get" action="">
	
		<div class="form-group">
			<label for="buscar" class="col-xs-2 control-label">Buscar por
				nome:</label>
			<div class="col-xs-4">
				<input Type="text" class="form-control" id="buscar" name="q" />
			</div>
			<button type="submit" class="btn btn-default">Pesquisar</button>
		</div>
	</form>
	
	<a href="/fdw/cursos/cadastro" class="btn btn-default" style="margin-bottom: 15px">Cadastrar Curso</a>
	
	<table
		class="table table-bordered table-hover table-condensed table-responsive">
		<thead>
			<tr>
				<th>Nome</th>
				<th>Tipo</th>
				<th class="acoes">A��es</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${cursos}" var="curso">
				<tr>
					<td>${curso.nome}</td>
					<td>${curso.tipo}</td>
					<td class="acoes">
						<a href="${pageContext.request.contextPath}/cursos/editar/${curso.id}"><span class="glyphicon glyphicon-pencil" aria-hidden=true></span></a>
						<a href="${pageContext.request.contextPath}/cursos/excluir/${curso.id}"><span class="glyphicon glyphicon-trash" aria-hidden=true></span></a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
<%@include file="/WEB-INF/parts/footer.jsp"%>