<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/WEB-INF/parts/header.jsp"%>
	<h1>Listagem de Disciplinas</h1>
	
	<form class="form-horizontal" action="" method="get">
	
		<div class="form-group">
			<label for="buscar" class="col-xs-2 control-label">Buscar por
				nome:</label>
			<div class="col-xs-4">
				<input Type="text" class="form-control" id="buscar" name="q" />
			</div>
			<button type="submit" class="btn btn-default">Pesquisar</button>
		</div>
	</form>
	<a href="${pageContext.request.contextPath}/disciplinas/cadastro"
				class="btn btn-default">Novo</a>
				<br/>
	<table
		class="table table-bordered table-hover table-condensed table-responsive">
		<thead>
			<tr>
				<th>Nome</th>
				<th>Curso</th>
				<th>Area</th>
				<th class="acoes">A��es</th>
			</tr>
		</thead>
		<tbody>
			<c:if test="${empty disciplinas}">
				<tr>
					<td colspan="3" style="text-align: center">Nenhuma disciplina
						encontrada</td>
				</tr>
			</c:if>
			<c:if test="${not empty disciplinas}">
				<c:forEach items="${disciplinas}" var="d">
					<tr>
						<td>${d.nome}</td>
						<td>${d.curso.nome}</td>
						<td>${d.area}</td>
						<td class="acoes">
							<a href="${pageContext.request.contextPath}/disciplinas/editar/${d.id}"><span class="glyphicon glyphicon-pencil" aria-hidden=true></span></a>
							<a href="${pageContext.request.contextPath}/disciplinas/excluir/${d.id}"><span class="glyphicon glyphicon-trash" aria-hidden=true></span></a>
						</td>
					</tr>
				</c:forEach>
			</c:if>
		</tbody>
	</table>
<%@include file="/WEB-INF/parts/footer.jsp"%>