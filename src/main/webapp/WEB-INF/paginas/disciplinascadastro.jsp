<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@include file="/WEB-INF/parts/header.jsp"%>
	<h1>Inser��o de disciplina</h1>
	<form:form class="form-horizontal" method="post"
		action="${pageContext.request.contextPath}/disciplinas/cadastro" modelAttribute="disciplinaCadastro">
	
		<div class="form-group">
			<label for="dNome" class="col-xs-2 control-label">Nome:</label>
			<div class="col-xs-4">
				<form:input  path="nome" class="form-control" id="dNome" value="${disciplinaEditar.nome}"/>
         		<form:errors path="nome" />
			</div>
		</div>
	
		<div class="form-group">
			<label for="dArea" class="col-xs-2 control-label">Area:</label>
			<div class="col-xs-4">
				<form:input  path="area" class="form-control" id="dArea" value="${disciplinaEditar.area}"/>
         		<form:errors path="area" />
			</div>
		</div>
	
		<div class="form-group">
			<label for="curso" class="col-xs-2 control-label">Curso:</label>
			<div class="col-xs-5">
				<select class="form-control" name="cursoid" value="${disciplinaEditar.curso.id}">
					<c:if test="${empty cursos}">
						<option>Nenhum curso cadastrado</option>
					</c:if>
					<c:if test="${not empty cursos}">
						<c:forEach items="${cursos}" var="c">
							<option value="${c.id}">${c.nome}</option>
						</c:forEach>
					</c:if>
				</select>
				
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-6"></div>
			<button type="submit" class="btn btn-default">Salvar</button>
	
		</div>
	</form:form>
<%@include file="/WEB-INF/parts/footer.jsp"%>