<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Controle AcadÍmico</title>
        <!--define a viewport--->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8">
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/favicon.png">
        <!--adicionar CSS Bootstrap -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <!--Estilo personalizado -->
        <link href="${pageContext.request.contextPath}/css/estilo.css" rel="stylesheet" media="screen">
    </head>
    <body>
        <div class="linhaPreto">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    
                <p class="corbranco text-center">Controle AcadÍmico</p>
                
                </div>
           </div>
        </div>
        </div>    
        <hr />
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                
                <c:if test="${not empty msgErro}">
					<div class="alert alert-danger">${msgErro}</div>
				</c:if>
				<c:if test="${not empty msgInfo}">
					<div class="alert alert-info">${msgInfo}</div>
				</c:if>
				<c:if test="${not empty msgNotif}">
					<div class="alert alert-info">${msgNotif} <a href="${pageContext.request.contextPath}/marcarcomolida">Marcar como Lida</a></div>
				</c:if>
				<c:if test="${not empty msgSucesso}">
					<div class="alert alert-success">${msgSucesso}</div>
				</c:if>
                   
                    <form class="form-horizontal" action="/fdw/logar" method="post">
                        
                        <div class="form-group">
                            <label for="login" class="col-xs-4 control-label">Login:</label>
                            <div class="col-xs-5"><input type="text" name="login" class="form-control" id="login" placeholder="Usuario"/></div>
                        </div>
                        <div class="form-group">
                            <label for="senha" class="col-xs-4 control-label">Senha:</label>
                           <div class="col-xs-5"><input type="password" name="senha" class="form-control" id="senha" placeholder="Senha"/></div>
                        
                        </div>    
                       
                        <div class="form-group">
                            <div class="col-xs-offset-2 col-xs-6"></div>
                            <button type="submit" class="btn btn-default">Entrar</button>
                            
                        </div>

                    </form>
                </div>
            </div>
            </div>
        <div class="linhaPreto2 footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <p class="corbranco2 text-center">Densenvolvido pelos os alunos de CC</p>
                </div>
            </div>
        </div>
        </div>


        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
       
</body>

</html>