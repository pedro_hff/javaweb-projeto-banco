<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@include file="/WEB-INF/parts/header.jsp"%>
	<h1>Matricular Aluno: ${aluno.nome}</h1>
	
	<form:form class="form-horizontal" method="post"
		action="${pageContext.request.contextPath}/alunos/matricular">
		<div class="form-group">
			<label for="curso" class="col-xs-2 control-label">Turma:</label>
			<div class="col-xs-5">
				<select class="form-control" name="turmaid">
					<c:if test="${empty turmas}">
						<option>Nenhum curso cadastrado</option>
					</c:if>
					<c:if test="${not empty turmas}">
						<c:forEach items="${turmas}" var="c">
							<option value="${c.id}">${c.ano}.${c.semestre} - ${c.disciplina.nome}</option>
						</c:forEach>
					</c:if>
				</select>
				
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-6"></div>
			<button type="submit" class="btn btn-default">Salvar</button>
	
		</div>
	</form:form>
<%@include file="/WEB-INF/parts/footer.jsp"%>