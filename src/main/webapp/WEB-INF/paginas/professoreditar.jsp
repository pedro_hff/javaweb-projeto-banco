<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/WEB-INF/parts/header.jsp"%>
	<h1>Editar Professor</h1>
	
	<form class="form-horizontal" method="post"
		action="/FileiDeWanderlyWeb/professores/editar">
	
		<div class="form-group">
			<label for="n_professor" class="col-xs-2 control-label">Nome:</label>
			<div class="col-xs-4">
				<input Type="text" class="form-control" id="n_professor" name="nome"
					value="${professor.nome}" />
			</div>
		</div>
		<div class="form-group">
			<label for="cpf" class="col-xs-2 control-label">Cpf:</label>
			<div class="col-xs-4">
				<input Type="text" class="form-control" id="cpf" name="cpf"
					value="${professor.cpf}" />
			</div>
	
		</div>
		<div class="form-group">
			<label for="rg" class="col-xs-2 control-label">RG:</label>
			<div class="col-xs-4">
				<input Type="text" class="form-control" id="rg" name="rg"
					value="${professor.rg}" />
			</div>
	
		</div>
		<div class="form-group">
			<label for="areaAtuacao" class="col-xs-2 control-label">Area de Atuacao:</label>
			<div class="col-xs-4">
				<input Type="text" class="form-control" id="areaAtuacao" name="areaAtuacao"
					value="${professor.areaAtuacao}" />
			</div>
		</div>
		</div>
			<div class="form-group">
			<label for="titulacao" class="col-xs-2 control-label">Titulacao:</label>
			<div class="col-xs-4">
				<input Type="text" class="form-control" id="titulacao" name="titulacao"
					value="${professor.titulacao}" />
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-xs-6"></div>
			<button type="submit" class="btn btn-default">Salvar</button>
	
		</div>
	</form>
<%@include file="/WEB-INF/parts/footer.jsp"%>