<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/WEB-INF/parts/header.jsp"%>

	<h1>Listagem de Professores</h1>
	
	<form class="form-horizontal">
	
		<div class="form-group">
			<label for="buscar" class="col-xs-2 control-label">Buscar por
				nome:</label>
			<div class="col-xs-4">
				<input Type="text" class="form-control" id="buscar" name="q" />
			</div>
			<button type="submit" class="btn btn-default">Pesquisar</button>
		</div>
	</form>
	
	<a href="${pageContext.request.contextPath}/professores/cadastro" class="btn btn-default" style="margin-bottom: 15px">Novo Professor</a>
	
	<table
		class="table table-bordered table-hover table-condensed table-responsive">
		<thead>
			<tr>
				<th>Nome</th>
				<th>CPF</th>
				<th>RG</th>
				<th>Area de Atuacao</th>
				<th>Titulacao</th>
				<th class="acoes">A��es</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${professores}" var="professor">
				<tr>
					<td>${professor.nome}</td>
					<td>${professor.cpf}</td>
					<td>${professor.rg}</td>
					<td>${professor.areaAtuacao}</td>
					<td>${professor.titulacao}</td>
					<td class="acoes">
						<a href="${pageContext.request.contextPath}/professores/matricular/${professor.id}">Matricular</a>
						<a href="${pageContext.request.contextPath}/professores/editar/${professor.id}"><span class="glyphicon glyphicon-pencil" aria-hidden=true></span></a>
						<a href="${pageContext.request.contextPath}/professores/excluir/${professor.id}"><span class="glyphicon glyphicon-trash" aria-hidden=true></span></a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
<%@include file="/WEB-INF/parts/footer.jsp"%>