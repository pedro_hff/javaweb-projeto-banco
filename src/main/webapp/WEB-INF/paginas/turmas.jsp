<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/WEB-INF/parts/header.jsp"%>
	<h1>Listagem de Turmas</h1>
		<form class="form-horizontal">
		
			<div class="form-group">
				<label for="curso" class="col-xs-2 control-label">Busca por curso:</label>
				<div class="col-xs-3">
					<select class="form-control" name="q">
						<c:forEach items="${cursosBusca}" var="curso">
							<option value="${curso.id}">${curso.nome}</option>
						</c:forEach>
					</select>
				</div>
				<div class="col-xs-1">
					<button type="submit" class="btn btn-default">Pesquisar</button>
				</div>
			</div>
		</form>
		
		<a href="${pageContext.request.contextPath}/turmas/cadastro" class="btn btn-default" style="margin-bottom:15px">Novo</a>
		<c:if test="${not empty turmas}">
		<table
			class="table table-bordered table-hover table-condensed table-responsive">
			<thead>
				<tr>
					<th>Periodo</th>
					<th>Disciplina</th>
					<th>Curso</th>
					<th>Professor</th>
					<th class="acoes">A��es</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${empty turmas}">
					<tr>
						<td colspan="5" style="text-align: center">Nenhuma turma encontrada</td>
					</tr>
				</c:if>
				<c:if test="${not empty turmas}">
					<c:forEach items="${turmas}" var="t">
						<tr>
							<td>${t.ano}.${t.semestre}</td>
							<td>${t.disciplina.nome}</td>
							<td>${t.disciplina.curso.nome}</td>
							<td>${t.professor.nome}</td>
							<td class="acoes">
								<a href="${pageContext.request.contextPath}/turmas/editar/${t.id}"><span class="glyphicon glyphicon-pencil" aria-hidden=true></span></a>
								<a href="${pageContext.request.contextPath}/turmas/excluir/${t.id}"><span class="glyphicon glyphicon-trash" aria-hidden=true></span></a>
							</td>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
	</c:if>
	<c:if test="${not empty turmasAluno}">
		<table
			class="table table-bordered table-hover table-condensed table-responsive">
			<thead>
				<tr>
					<th>Periodo</th>
					<th>Disciplina</th>
					<th>Curso</th>
					<th>Professor</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${empty turmasAluno}">
					<tr>
						<td colspan="5" style="text-align: center">Nenhuma turma encontrada</td>
					</tr>
				</c:if>
				<c:if test="${not empty turmasAluno}">
					<c:forEach items="${turmasAluno}" var="t">
						<tr>
							<td>${t.ano}.${t.semestre}</td>
							<td>${t.disciplina.nome}</td>
							<td>${t.disciplina.curso.nome}</td>
							<td>${t.professor.nome}</td>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
	</c:if>
	
<%@include file="/WEB-INF/parts/footer.jsp"%>