<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/WEB-INF/parts/header.jsp"%>
	<h1>Inser��o/edi��o de Turmas</h1>
	
	<form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/turmas/cadastro">
	
		<div class="form-group">
			<label for="ano" class="col-xs-2 control-label">Ano:</label>
			<div class="col-xs-5">
				<select class="form-control" name="ano" required>
					<option>Selecione ...</option>
					<option>2017</option>
					<option>2018</option>
					<option>2019</option>
					<option>2020</option>
					<option>2021</option>
					<option>2022</option>
	
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="periodo" class="col-xs-2 control-label">Semestre:</label>
			<div class="col-xs-5">
				<select class="form-control" name="semestre" required>
					<option>Selecione ...</option>
					<option>1</option>
					<option>2</option>
				</select>
			</div>
		</div>
	
	
		<div class="form-group">
			<label for="curso" class="col-xs-2 control-label">Disciplina:</label>
			<div class="col-xs-5">
				<select class="form-control" name="disciplinaid" required>
					<c:if test="${empty disciplinas}">
						<option>Nenhuma disciplina cadastrada</option>
					</c:if>
					<c:if test="${not empty disciplinas}">
						<c:forEach items="${disciplinas}" var="d">
							<option value="${d.id}">${d.curso.nome} - ${d.nome}</option>
						</c:forEach>
					</c:if>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="professor" class="col-xs-2 control-label">Professor:</label>
			<div class="col-xs-5">
				<select class="form-control" name="professornome" required>
					<option>Selecione ...</option>
					<option>Francisco Porf�rio</option>
					<option>Eduardo Ribas</option>
					<option>Humberto Rocha</option>
	
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-6"></div>
			<button type="submit" class="btn btn-default">Salvar</button>
	
		</div>
	</form>
<%@include file="/WEB-INF/parts/footer.jsp"%>