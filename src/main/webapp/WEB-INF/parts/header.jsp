<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Listagem de Curso      </title>
        <!--define a viewport--->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8">
        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/favicon.png">
        <!--adicionar CSS Bootstrap -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen">
        <!--Estilo personalizado -->
        <link href= "${pageContext.request.contextPath}/css/estilo.css" rel="stylesheet" type="text/css" media="screen">
    </head>
    <body>
        <div class="linhaPreto">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-12 ">                    
	                	<p class="corbranco3">Controle AcadÍmico</p>
	                </div>
	           </div>
	        </div>
        </div>    
        <div class="container">
        	<%@include file="/WEB-INF/parts/menu.jsp"%>
        </div>
        <div class="container">
        
        <c:if test="${not empty msgErro}">
			<div class="alert alert-danger">${msgErro}</div>
		</c:if>
		<c:if test="${not empty msgInfo}">
			<div class="alert alert-info">${msgInfo}</div>
		</c:if>
		<c:if test="${not empty msgNotif}">
			<div class="alert alert-danger">${msgNotif} <a href="${pageContext.request.contextPath}/marcarcomolida">Marcar como Lida</a></div>
		</c:if>
		<c:if test="${not empty msgSucesso}">
			<div class="alert alert-success">${msgSucesso}</div>
		</c:if>