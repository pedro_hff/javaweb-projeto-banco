<nav class="navbar navbar-default">
	<div>
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
				aria-expanded="false">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>


		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<% if (((Integer) session.getAttribute("tipo")) == 0) { %>
					<li><a href="${pageContext.request.contextPath}/cursos">Curso </a></li>
					<li><a href="${pageContext.request.contextPath}/disciplinas">Disciplina </a></li>
					<li><a href="${pageContext.request.contextPath}/turmas">Turmas</a></li>
					<li><a href="${pageContext.request.contextPath}/alunos">Alunos</a></li>
					<li><a href="${pageContext.request.contextPath}/professores">Professores</a></li>
				<% } %>

			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li><a href="#">Bem vindo ${usuario.nome}!</a></li>
				<li><a href="${pageContext.request.contextPath}/logout">Sair</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid -->
</nav>